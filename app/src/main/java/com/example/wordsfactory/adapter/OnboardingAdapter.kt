package com.example.wordsfactory.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsfactory.R
import com.example.wordsfactory.adapter.OnboardingAdapter.ViewHolder
import com.example.wordsfactory.databinding.WordingPageBinding
import com.example.wordsfactory.model.OnboardingData


class OnboardingAdapter(
    private val context: Context,
    private val onboardingPages: List<OnboardingData>

) : RecyclerView.Adapter<ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.wording_page, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding=WordingPageBinding.bind(holder.itemView)
        val pages=onboardingPages[position]

        binding.padinationImage.setImageResource(pages.image)
        binding.titleText.text=pages.title
        binding.subtitleText.text=pages.subtitle
        binding.padinationImage.setImageResource(pages.points)

    }

    override fun getItemCount()=onboardingPages.size

}




