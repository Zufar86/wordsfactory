package com.example.wordsfactory.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wordsfactory.R
import com.example.wordsfactory.databinding.MeaningsTextBinding
import com.example.wordsfactory.model.Definitions


class MeaningsAdapter(
    private val context: Context,
    private val definitions: List<Definitions>
) : RecyclerView.Adapter<MeaningsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.meanings_text, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val binding = MeaningsTextBinding.bind(holder.itemView)
        val definition = definitions[position]

        binding.meaningsText.text = definition.definition
        binding.exampleText.text = "Example: ${definition.example}"
    }

    override fun getItemCount()=definitions.size

}




