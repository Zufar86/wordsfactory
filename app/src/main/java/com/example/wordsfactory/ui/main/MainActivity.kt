package com.example.wordsfactory.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.wordsfactory.R
import com.example.wordsfactory.databinding.ActivityMainBinding
import com.example.wordsfactory.ui.fragment.FragmentDictionary
import com.example.wordsfactory.ui.fragment.WebFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            if (item.itemId == R.id.nav_Dictionary)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, FragmentDictionary())
                    .commit()
            else if (item.itemId == R.id.nav_Video)
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragmentContainer, WebFragment())
                    .commit()

            true
        }

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, FragmentDictionary())
            .commit()

    }
}