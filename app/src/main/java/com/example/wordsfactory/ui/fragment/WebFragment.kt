package com.example.wordsfactory.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.wordsfactory.R
import com.example.wordsfactory.databinding.FragmettWebBinding

class WebFragment : Fragment(R.layout.fragmett_web) {

    private lateinit var binding: FragmettWebBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding=FragmettWebBinding.bind(view)

        binding.webView.loadUrl("https://learnenglish.britishcouncil.org/")
    }
}