package com.example.wordsfactory.ui.email

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.wordsfactory.databinding.ActivityEmailBinding
import com.example.wordsfactory.ui.main.MainActivity

class EmailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEmailBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=ActivityEmailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.singAppButton.setOnClickListener {
            val name=binding.nameText.text.toString()
            val email=binding.emailText.text.toString()
            val password=binding.passwordText.text.toString()

            if (name.isEmpty())
                binding.nameText.error="Введите имя"
            else if (email.isEmpty())
                binding.emailText.error="Введите имейл"
            else if (!email.contains("@") && !email.contains("."))
                binding.emailText.error="Введите корректный иимейл"
            else if (password.isEmpty())
                binding.passwordText.error="Введите пароль"
            else {
                startActivity(
                    Intent(this, MainActivity::class.java)
                )
                finish()
            }

        }

    }
}