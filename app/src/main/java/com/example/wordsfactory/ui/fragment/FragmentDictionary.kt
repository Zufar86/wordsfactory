package com.example.wordsfactory.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wordsfactory.R
import com.example.wordsfactory.adapter.MeaningsAdapter
import com.example.wordsfactory.data.NetworkManager
import com.example.wordsfactory.databinding.FragmentDictionariBinding
import com.example.wordsfactory.model.WordInfo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentDictionary : Fragment(R.layout.fragment_dictionari) {

    private lateinit var binding: FragmentDictionariBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding=FragmentDictionariBinding.bind(view)
        binding.meaningsRecycler.layoutManager = LinearLayoutManager(context)

        binding.searchWordsButton.setOnClickListener {
            val word=binding.serchEditText.text.toString()
            if (word.isNotEmpty()) {

                binding.waitConstr.visibility=View.GONE
                binding.aboutWordsInf.visibility=View.VISIBLE

                NetworkManager.instance.getWordInfo(word)
                    .enqueue(object : Callback<List<WordInfo>> {
                        override fun onResponse(
                            call: Call<List<WordInfo>>,
                            response: Response<List<WordInfo>>
                        ) {
                            if (response.body() != null) {
                                val word=response.body()!![0]

                                binding.wordsText.text=word.word
                                binding.transcriptionText.text=word.phonetic
                                binding.speechText.text=word.meanings[0].partOfSpeech

                                binding.meaningsRecycler.adapter = MeaningsAdapter(
                                    requireContext(),
                                    word.meanings[0].definitions
                                )
                            }
                        }

                        override fun onFailure(call: Call<List<WordInfo>>, t: Throwable) {
                            t.printStackTrace()

                        }
                    })


            }

        }


    }

}