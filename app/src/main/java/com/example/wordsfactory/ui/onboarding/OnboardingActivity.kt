package com.example.wordsfactory.ui.onboarding

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.wordsfactory.R
import com.example.wordsfactory.`object`.OnboardingRes
import com.example.wordsfactory.adapter.OnboardingAdapter
import com.example.wordsfactory.databinding.ActivityOnboardingBinding
import com.example.wordsfactory.ui.email.EmailActivity

class OnboardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnboardingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding=ActivityOnboardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.onboardingViewPager.orientation=ViewPager2.ORIENTATION_HORIZONTAL
        binding.onboardingViewPager.adapter=
            OnboardingAdapter(this, OnboardingRes.onboardingPageList)


        binding.nextBottom.setOnClickListener {
            if (binding.onboardingViewPager.currentItem != binding.onboardingViewPager.adapter!!.itemCount - 1)
                binding.onboardingViewPager.currentItem+=1
            else {
                startActivity(
                    Intent(this, EmailActivity::class.java)
                )
                finish()
            }
        }
        binding.onboardingViewPager.registerOnPageChangeCallback(
            object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    if (position == binding.onboardingViewPager.adapter!!.itemCount - 1)
                        binding.nextBottom.text = getString(R.string.stringLetsStart)
                    else
                        binding.nextBottom.text = getString(R.string.stringNextText)
                }
            }
        )


        binding.skipBottom.setOnClickListener {
            startActivity(
                Intent(this, EmailActivity::class.java)
            )
            finish()
        }


    }
}