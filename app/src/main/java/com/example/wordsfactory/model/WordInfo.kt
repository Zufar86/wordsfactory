package com.example.wordsfactory.model

data class WordInfo(
    val word: String,
    val phonetic: String,
    val meanings: List<Meaning>
)


