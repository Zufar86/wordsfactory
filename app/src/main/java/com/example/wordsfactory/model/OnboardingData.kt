package com.example.wordsfactory.model

data class OnboardingData(
    val image: Int,
    val title: String,
    val subtitle: String,
    val points: Int

)
