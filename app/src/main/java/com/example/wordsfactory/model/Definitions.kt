package com.example.wordsfactory.model

data class Definitions(
    val definition: String,
    val example: String

)