package com.example.wordsfactory.data

import com.example.wordsfactory.model.WordInfo
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface NetworkManager {

    companion object {
        val instance=Retrofit.Builder()
            .baseUrl("https://api.dictionaryapi.dev/api/v2/entries/en/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NetworkManager::class.java)

    }

    @GET("{word}")
    fun getWordInfo(@Path("word") word: String): Call<List<WordInfo>>

}