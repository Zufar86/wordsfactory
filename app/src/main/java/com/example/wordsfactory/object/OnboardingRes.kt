package com.example.wordsfactory.`object`

import com.example.wordsfactory.R
import com.example.wordsfactory.model.OnboardingData

object OnboardingRes {
    val onboardingPageList=listOf(
        OnboardingData(
            R.drawable.illustration_1,
            "Learn anytime and anywhere",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.pagination_1
        ),
        OnboardingData(
            R.drawable.illustration_2,
            "Find a course for you",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.pagination_2
        ),
        OnboardingData(
            R.drawable.illustration_3,
            "Improve your skills",
            "Quarantine is the perfect time to spend your day learning something new, from anywhere!",
            R.drawable.pagination_3
        )

    )
}